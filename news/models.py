from calendar import monthrange
from django.db import models
from datetime import date
from datetime import datetime
from django.utils.encoding import python_2_unicode_compatible
from django.utils.timezone import UTC


class INewsItem(models.Model):
    Title = models.CharField(max_length=150)

    #these three fields are for event items
    FreeFormDate = models.CharField(blank=True, null=True, max_length=30, verbose_name='Date/Range')
    FreeFormTime = models.CharField(blank=True, null=True, max_length=30, verbose_name='Time')
    Location = models.CharField(blank=True, null=True, max_length=60)

    IsEvent = models.BooleanField(verbose_name='is event?', default=False)

    #these two fields are for news items
    PubDate = models.DateField(verbose_name='date',
                               default=date.today,
                               help_text='Enter date in YYYY-MM-DD (year-month-day) format',
                               error_messages={'invalid': 'Enter date in YYYY-MM-DD (year-month-day) format',
                                               'required': 'Date required: enter date in YYYY-MM-DD (year-month-day) format'})
    Time = models.TimeField(blank=True, null=True,
                            default=(lambda: datetime.now().time()),
                            help_text='Enter time in HH:MM:SS (hours-minutes-seconds) format',
                            error_messages={'invalid': 'Enter time in HH:MM:SS (hours-minutes-seconds) format'})

    Blurb = models.TextField(blank=True)
    Featured = models.BooleanField(verbose_name='is featured?')

    class Meta:
        app_label = 'v1'
        db_table = 'News_newsitem'


class IPhoto(models.Model):
    Item = models.ForeignKey(INewsItem)
    Image = models.ImageField(upload_to='news/photos')
    Title = models.CharField(max_length=100)
    Caption = models.CharField(max_length=250, blank=True, verbose_name='caption')
    class Meta:
        app_label = 'v1'
        db_table = 'News_photo'


class IAttachedFile(models.Model):
    Item = models.ForeignKey(INewsItem)
    File = models.FileField(upload_to='news/files')
    Description = models.CharField(max_length=250, blank=True)
    class Meta:
        app_label = 'v1'
        db_table = 'News_attachedfile'


class IEvent(models.Model):
    Title = models.CharField(max_length=200)
    Body = models.TextField()
    End = models.DateField(verbose_name='date',
                           default=date.today,
                           help_text='Enter date in YYYY-MM-DD (year-month-day) format',
                           error_messages={'invalid': 'Enter date in YYYY-MM-DD (year-month-day) format',
                                           'required': 'Date required: enter date in YYYY-MM-DD (year-month-day) format'})
    class Meta:
        app_label = 'v1'
        db_table = 'News_event'


class ISlideShow(models.Model):
    Title = models.CharField(max_length=200)
    Image = models.ImageField(upload_to='public/images/slideshow')
    Url = models.URLField(null=True, blank=True)
    Forever = models.BooleanField(blank=True)
    End = models.DateField(verbose_name='date',
                           default=date.today,
                           help_text='Enter date in YYYY-MM-DD (year-month-day) format',
                           error_messages={'invalid': 'Enter date in YYYY-MM-DD (year-month-day) format',
                                           'required': 'Date required: enter date in YYYY-MM-DD (year-month-day) format'})
    class Meta:
        app_label = 'v1'
        db_table = 'News_slideshow'


@python_2_unicode_compatible
class NewsItem(models.Model):
    title = models.CharField(max_length=150)
    date = models.CharField(blank=True, null=True, max_length=30, verbose_name='Date/Range')
    time = models.CharField(blank=True, null=True, max_length=30, verbose_name='Time')
    location = models.CharField(blank=True, null=True, max_length=60)
    is_event = models.BooleanField(verbose_name='is event?', default=False)
    pub_date = models.DateTimeField(verbose_name='date',
                                    default=datetime.utcnow,
                                    help_text='Enter date in YYYY-MM-DD MM:SS (year-month-day minute:second) format',
                                    error_messages={
                                    'invalid': 'Enter date in YYYY-MM-DD (year-month-day minute:second) format',
                                    'required': 'Date required: enter date in YYYY-MM-DD (year-month-day) format'})
    blurb = models.TextField(blank=True)
    featured = models.BooleanField(verbose_name='is featured?')

    QUARTERS = ((1, 3), (4, 6), (7, 9), (10, 12))
    QUARTERS_LABELS = ('Jan - Mar', 'Apr - Jun', 'Jul - Sept', 'Oct - Dec')

    class Meta:
        ordering = ['-pub_date']
        db_table = 'aku_news_item'

    @staticmethod
    def by_quarter(quarter):
        assert 0 <= quarter < 4, 'The quarter must be 0-3.'
        now = datetime.now()
        from_date = datetime(now.year, NewsItem.QUARTERS[quarter][0], 1, tzinfo=UTC())
        last_day = monthrange(now.year, NewsItem.QUARTERS[quarter][1])
        to_date = datetime(now.year, NewsItem.QUARTERS[quarter][1], last_day[1], tzinfo=UTC())
        return NewsItem.objects.filter(is_event=False, pub_date__gte=from_date, pub_date__lte=to_date)

    @staticmethod
    def get_quarter(month):
        assert 0 < month < 13, 'The month must be between 1 and 12.'
        if 0 < month <= 3:
            return 0
        if 4 < month <= 6:
            return 1
        if 7 < month <= 9:
            return 2
        if 10 < month <= 12:
            return 3

    def __str__(self):
        return '{0} ({1})'.format(self.title.encode('utf-8'), self.pub_date)


@python_2_unicode_compatible
class Photo(models.Model):
    item = models.ForeignKey(NewsItem)
    image = models.ImageField(upload_to='news/photos')
    title = models.CharField(max_length=100)
    caption = models.CharField(max_length=250, blank=True, verbose_name='caption')

    class Meta:
        ordering = ['title']
        db_table = 'aku_photo'

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class AttachedFile(models.Model):
    item = models.ForeignKey(NewsItem)
    file = models.FileField(upload_to='news/files')
    description = models.CharField(max_length=250, blank=True)

    class Meta:
        ordering = ['file']
        db_table = 'aku_attached_file'

    def __str__(self):
        return self.description


@python_2_unicode_compatible
class Event(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    end = models.DateField(verbose_name='date',
                           default=datetime.utcnow,
                           help_text='Enter date in YYYY-MM-DD (year-month-day) format',
                           error_messages={'invalid': 'Enter date in YYYY-MM-DD (year-month-day) format',
                                           'required': 'Date required: enter date in YYYY-MM-DD (year-month-day) format'})

    class Meta:
        ordering = ['-end']
        db_table = 'aku_event'

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class SlideShow(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='public/images/slideshow')
    url = models.URLField(null=True, blank=True)
    forever = models.BooleanField(blank=True)
    end = models.DateField(verbose_name='date',
                           default=datetime.utcnow,
                           help_text='Enter date in YYYY-MM-DD (year-month-day) format',
                           error_messages={'invalid': 'Enter date in YYYY-MM-DD (year-month-day) format',
                                           'required': 'Date required: enter date in YYYY-MM-DD (year-month-day) format'})

    class Meta:
        ordering = ['end']
        db_table = 'aku_slide_show'

    def __str__(self):
        return self.title
