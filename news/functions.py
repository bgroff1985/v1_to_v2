from news.models import INewsItem, NewsItem, IPhoto, Photo, IAttachedFile, AttachedFile, IEvent, Event, SlideShow, ISlideShow
from pytz import timezone
from datetime import datetime

hst = timezone('Pacific/Honolulu')


def import_news():
    import_news_items()
    import_news_photo()
    import_news_file()
    import_events()
    import_slideshow()


def import_news_items():
    for item in INewsItem.objects.all():
        NewsItem.objects.get_or_create(pk=item.pk, defaults={
            'title': item.Title,
            'date': item.PubDate,
            'time': item.Time,
            'location': item.Location,
            'is_event': item.IsEvent,
            'blurb': item.Blurb,
            'pub_date': datetime(item.PubDate.year, item.PubDate.month, item.PubDate.day, tzinfo=hst),
            'featured': item.Featured,
        })


def import_news_photo():
    for photo in IPhoto.objects.all():
        Photo.objects.get_or_create(pk=photo.pk, defaults={
            'item_id': photo.Item.id,
            'image': photo.Image,
            'title': photo.Title,
            'caption': photo.Caption,
        })


def import_news_file():
    for attached_file in IAttachedFile.objects.all():
        AttachedFile.objects.get_or_create(pk=attached_file.pk, defaults={
            'item_id': attached_file.Item.id,
            'file': attached_file.File,
            'description': attached_file.Description,
        })


def import_events():
    for event in IEvent.objects.all():
        Event.objects.get_or_create(pk=event.pk, defaults={
            'title': event.Title,
            'body': event.Body,
            'end': event.End,
        })


def import_slideshow():
    for slide in ISlideShow.objects.all():
        SlideShow.objects.get_or_create(pk=slide.pk, defaults={
            'title': slide.Title,
            'image': slide.Image,
            'url': slide.Url,
            'forever': slide.Forever,
            'end': slide.End,
        })