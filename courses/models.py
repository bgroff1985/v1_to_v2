from accounts.models import IPerson
from django.db import models


class ICourseType(models.Model):
    Type = models.CharField(max_length=100)
    Code = models.CharField(max_length=2, unique=True)

    class Meta:
        db_table = 'Course_coursetype'
        app_label = 'v1'


class ITrainingType(models.Model):
    Type = models.CharField(max_length=100)
    Code = models.CharField(max_length=2, unique=True)

    class Meta:
        db_table = 'Course_trainingtype'
        app_label = 'v1'


class IDeliveryType(models.Model):
    Type = models.CharField(max_length=20)
    Code = models.CharField(max_length=2, unique=True)

    class Meta:
        db_table = 'Course_deliverytype'
        app_label = 'v1'


class ITrainingMethod(models.Model):
    Type = models.CharField(max_length=100)
    Code = models.CharField(max_length=1, unique=True)

    class Meta:
        db_table = 'Course_trainingmethod'
        app_label = 'v1'


class ITrainingProvider(models.Model):
    ProviderID = models.CharField(max_length=25, unique=True)
    Phone = models.CharField(max_length=10)
    Email = models.CharField(max_length=100)

    class Meta:
        db_table = 'Course_trainingprovider'
        app_label = 'v1'


class IDocumentType(models.Model):
    Code = models.CharField(max_length=3, unique=True)
    Name = models.CharField(max_length=50)

    class Meta:
        db_table = 'Course_documenttype'
        app_label = 'v1'


class IDocument(models.Model):
    Name = models.CharField(max_length=100)
    File = models.FileField(max_length=255)
    MIMEType = models.CharField(max_length=100)
    Version = models.CharField(max_length=255)
    DocumentType = models.ForeignKey(IDocumentType)
    Course = models.ForeignKey('ICourse')

    class Meta:
        db_table = 'Course_document'
        app_label = 'v1'


class ICourse(models.Model):
    CourseName = models.CharField(max_length=80)
    ShortName = models.CharField(max_length=50)
    CourseNumber = models.CharField(max_length=50)
    TrainingProvider = models.ForeignKey(ITrainingProvider)
    TrainingType = models.ForeignKey(ITrainingType)
    CourseType = models.ForeignKey(ICourseType)
    ModuleCount = models.IntegerField()
    ContactHours = models.CharField(max_length=5)
    Certified = models.BooleanField()
    Description = models.TextField(null=True, blank=True)
    TargetAudience = models.TextField(null=True, blank=True)
    Prerequisites = models.TextField(null=True, blank=True)
    Requirements = models.TextField(null=True, blank=True)
    Icon2d = models.FileField(upload_to='icons/', null=True, blank=True)
    Icon3d = models.FileField(upload_to='icons/', null=True, blank=True)
    Status = models.IntegerField(null=True)
    Featured = models.BooleanField(verbose_name='is featured?')

    class Meta:
        db_table = 'Course_course'
        app_label = 'v1'


class ITestType(models.Model):
    Type = models.CharField(max_length=4, unique=True)

    class Meta:
        db_table = 'Course_testtype'
        app_label = 'v1'


class ITest(models.Model):
    Course = models.ForeignKey(ICourse)
    EffectiveDate = models.DateField()
    Type = models.ForeignKey(ITestType)
    Label = models.CharField(max_length=1000)

    class Meta:
        db_table = 'Course_test'
        app_label = 'v1'


class IQuestion(models.Model):
    Question = models.TextField()
    Test = models.ForeignKey(ITest)
    SortOrder = models.IntegerField()

    class Meta:
        db_table = 'Course_question'
        app_label = 'v1'


class IAnswer(models.Model):
    Question = models.ForeignKey(IQuestion)
    Answer = models.TextField()
    Correct = models.BooleanField(default=False)
    SortOrder = models.IntegerField()

    class Meta:
        db_table = 'Course_answer'
        app_label = 'v1'


class IDocumentType(models.Model):
    Code = models.CharField(max_length=3, unique=True)
    Name = models.CharField(max_length=50)

    class Meta:
        db_table = 'Course_documenttype'
        app_label = 'v1'


class IDocument(models.Model):
    Name = models.CharField(max_length=100)
    File = models.FileField(max_length=255)
    Version = models.CharField(max_length=255)
    MIMEType = models.CharField(max_length=255)
    DocumentType = models.ForeignKey('IDocumentType')
    Course = models.ForeignKey('ICourse')

    class Meta:
        db_table = 'Course_document'
        app_label = 'v1'


class IDocumentDownload(models.Model):
    Document = models.ForeignKey('IDocument')
    Person = models.ForeignKey(IPerson)
    DownloadDate = models.DateTimeField()

    class Meta:
        db_table = 'Course_documentdownloads'
        app_label = 'v1'


class IPhoto(models.Model):
    item = models.ForeignKey('Question')
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='tests/images')
    caption = models.CharField(max_length=250, blank=True, verbose_name='caption')

    class Meta:
        db_table = 'Course_photo'
        app_label = 'v1'
