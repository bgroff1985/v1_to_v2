from courses.models import ICourseType, ITrainingType, IDeliveryType, ITrainingMethod, ITrainingProvider, ICourse,\
    ITestType, ITest, IQuestion, IAnswer, IDocumentType, IDocument, IDocumentDownload
from ndptc.courses.models import CourseType, TrainingType, TrainingProvider, Course, TestType, Test, Question, Answer,\
    DocumentType, DocumentDownload, Document
from ndptc.deliveries.models import DeliveryType, TrainingMethod


def import_course_data():
    # import_course_types()
    # import_training_types()
    # import_delivery_types()
    # import_training_methods()
    # import_training_providers()
    import_courses()
    # import_test_types()
    import_tests()
    import_questions()
    import_answers()
    # import_document_types()
    import_documents()
    import_document_downloads()


def import_course_types():
    for course_type in ICourseType.objects.all():
        CourseType.objects.get_or_create(id=course_type.id, description=course_type.Type, code=course_type.Code)


def import_training_types():
    for training_type in ITrainingType.objects.all():
        TrainingType.objects.get_or_create(id=training_type.id, description=training_type.Type,
                                            code=training_type.Code)


def import_delivery_types():
    for delivery_type in IDeliveryType.objects.all():
        DeliveryType.objects.get_or_create(id=delivery_type.id, description=delivery_type.Type,
                                            code=delivery_type.Code)


def import_training_methods():
    for training_method in ITrainingMethod.objects.all():
        TrainingMethod.objects.get_or_create(id=training_method.id, description=training_method.Type,
                                              code=training_method.Code)


def import_training_providers():
    for training_provider in ITrainingProvider.objects.all():
        TrainingProvider.objects.get_or_create(id=training_provider.id, provider=training_provider.ProviderID,
                                                email=training_provider.Email, phone=training_provider.Phone)


def import_courses():
    for course in ICourse.objects.all():
        course_type = CourseType.objects.get(id=course.CourseType_id)
        training_provider = TrainingProvider.objects.get(id=course.TrainingProvider_id)
        training_type = TrainingType.objects.get(id=course.TrainingType_id)
        Course.objects.get_or_create(id=course.id, defaults={
            'certified': course.Certified,
            'contact_hours': course.ContactHours,
            'name': course.CourseName,
            'number': course.CourseNumber,
            'course_type': course_type,
            'description': course.Description,
            'icon2d': course.Icon2d,
            'icon3d': course.Icon3d,
            'prerequisites': course.Prerequisites,
            'requirements': course.Requirements,
            'short_name': course.ShortName,
            'status': course.Status,
            'target_audience': course.TargetAudience,
            'training_provider': training_provider,
            'training_type': training_type,
        })


def import_test_types():
    for test_type in ITestType.objects.all():
        TestType.objects.get_or_create(type=test_type.Type)


def import_tests():
    for test in ITest.objects.all():
        Test.objects.get_or_create(id=test.id, defaults={
            'type_id': test.Type.Type,
            'effective_date': test.EffectiveDate,
            'course_id': test.Course_id,
            'label': test.Label,
        })


def import_questions():
    for question in IQuestion.objects.all():
        Question.objects.get_or_create(id=question.id, defaults={
            'test_id': question.Test_id,
            'question': question.Question,
            'sort_order': question.SortOrder,
        })


def import_answers():
    for answer in IAnswer.objects.all():
        Answer.objects.get_or_create(id=answer.id, defaults={
            'question_id': answer.Question_id,
            'answer': answer.Answer,
            'correct': answer.Correct,
            'sort_order': answer.SortOrder,
        })


def import_document_types():
    for document_type in IDocumentType.objects.all():
        DocumentType.objects.get_or_create(id=document_type.id, defaults={
            'code': document_type.Code,
            'name': document_type.Name,
        })


def import_documents():
    for document in IDocument.objects.all():
        Document.objects.get_or_create(id=document.id, defaults={
            'name': document.Name,
            'file': document.File,
            'version': document.Version,
            'mime_type': document.MIMEType,
            'document_type_id': document.DocumentType_id,
            'course_id': document.Course_id,
        })


def import_document_downloads():
    for download in IDocumentDownload.objects.all():
        DocumentDownload.objects.get_or_create(id=download.id, defaults={
            'document_id': download.Document_id,
            'person_id': download.Person_id,
            'download_date': download.DownloadDate,
        })

