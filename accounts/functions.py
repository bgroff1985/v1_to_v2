from uuid import uuid4
from ndptc.accounts.models import DMSPerson
from .models import IUser, IPerson, MoiPerson, AkuPerson


def create_user(person, uh_user, user):
    if len(person.WorkAddress.Address) > 100:
        person.WorkAddress.Address = person.WorkAddress.Address[:100]
        person.WorkAddress.Address1 = person.WorkAddress.Address[100:]
    return DMSPerson.objects.get_or_create(id=person.id, defaults={
        'username': user.username,
        'password': user.password,
        'is_uh': uh_user,
        'is_active': user.is_active,
        'email': person.Email,
        'first_name': person.FirstName,
        'last_name': person.LastName,
        'middle': person.MiddleInitial,
        'title': person.PositionTitle,
        'agency': person.AgencyName,
        'address': person.WorkAddress.Address,
        'address1': person.WorkAddress.Address1,
        'city': person.WorkAddress.City,
        'state_id': person.WorkAddress.State_id,
        'country_id': person.WorkAddress.Country_id,
        'zip': person.WorkAddress.Zip,
        'phone': person.WorkPhone,
        'fema_id': person.FemaID,
        'citizen': person.USCitizen,
        'government_level_id': person.GovernmentLevel_id,
        'discipline_id': person.Discipline_id,
    })


def import_users():
    for user in IUser.objects.all():
        try:
            person = IPerson.objects.get(user=user)
        except IPerson.DoesNotExist:
            continue
        if person.Email is '':
            person.Email = person.FirstName + "@fake.com"

        if user.password == '!' and '@hawaii.edu' in user.email.lower():
            uh_user = True
        else:
            uh_user = False

        dms_person, created = create_user(person, uh_user, user)
        if created and user.is_staff:
            moi_person = MoiPerson()
            moi_person.__dict__ = dms_person.__dict__
            moi_person.dms_person = dms_person
            moi_person.is_admin = True if user.is_staff else False
            moi_person.save()

        if created:
            aku_person = AkuPerson()
            aku_person.__dict__ = dms_person.__dict__
            aku_person.dms_person = dms_person
            aku_person.is_admin = True if user.is_staff else False
            aku_person.is_staff = user.is_staff
            aku_person.is_active = user.is_active
            aku_person.save()

    for person in IPerson.objects.filter(user__isnull=True):
        user = IUser()
        user.username = str(uuid4())
        user.set_unusable_password()
        user.is_active = False
        user.is_generated = True
        create_user(person, False, user)
