from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from ndptc.accounts.models import DMSPerson
from utilities.models import IAddress, IGovernmentLevel, IDiscipline


class IUser(AbstractUser):
    class Meta:
        db_table = 'auth_user'
        app_label = 'v1'


class IPerson(models.Model):
    user = models.ForeignKey(IUser, related_name='User', null=True)
    LastName = models.CharField(max_length=50)
    FirstName = models.CharField(max_length=50)
    MiddleInitial = models.CharField(max_length=1, null=True)
    PositionTitle = models.CharField(max_length=100)
    AgencyName = models.CharField(max_length=100)
    WorkAddress = models.ForeignKey(IAddress, related_name='WorkAddress')
    MailingAddress = models.ForeignKey(IAddress, related_name='MailingAddress', null=True)
    WorkPhone = models.CharField(max_length=25)
    MobilePhone = models.CharField(max_length=25)
    GovernmentLevel = models.ForeignKey(IGovernmentLevel)
    Discipline = models.ForeignKey(IDiscipline)
    FemaID = models.CharField(max_length=10, null=True)
    USCitizen = models.BooleanField()
    Email = models.CharField(max_length=200)

    class Meta:
        db_table = 'Course_person'
        app_label = 'v1'


class AkuPerson(DMSPerson):

    class Meta:
        db_table = 'aku_person'

    objects = UserManager()


class MoiPerson(DMSPerson):
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    objects = UserManager()

    class Meta:
        db_table = 'moi_person'
