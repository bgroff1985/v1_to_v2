from django.db import models
from courses.models import ITrainingMethod, ICourse, IDeliveryType, IDocument, ITest, IQuestion, IAnswer, ITestType
from accounts.models import IPerson, IUser
from utilities.models import IAddress, ILanguage


class IDelivery(models.Model):
    Course = models.ForeignKey(ICourse)
    TrainingMethod = models.ForeignKey(ITrainingMethod)
    DeliveryType = models.ForeignKey(IDeliveryType)
    Location = models.ForeignKey(IAddress, null=True)
    Lat = models.FloatField()
    Lon = models.FloatField()
    StartDate = models.DateTimeField()
    EndDate = models.DateTimeField()
    FinishDate = models.DateField(null=True)
    ConfirmationCode = models.CharField(max_length=8, editable=False)
    Reported = models.BooleanField()
    ReportedDate = models.DateField(null=True)
    SequenceNumber = models.IntegerField(null=True)
    Status = models.IntegerField()
    HostingAgency = models.TextField(null=True, blank=True)
    MaximumEnrollment = models.IntegerField(null=True, blank=True)
    ContactInformation = models.TextField(null=True, blank=True)
    ArrivalTime = models.TextField(null=True, blank=True)
    CourseMaterials = models.TextField(null=True, blank=True)
    Internet = models.TextField(null=True, blank=True)
    Food = models.TextField(null=True, blank=True)
    Parking = models.TextField(null=True, blank=True)
    Requirements = models.TextField(null=True, blank=True)
    AdditionalInformation = models.TextField(null=True, blank=True)
    ReadingMaterials = models.TextField(null=True, blank=True)
    SpecialNote = models.TextField(null=True, blank=True)
    CertificateEmailsSent = models.BooleanField(default=False)
    RegistrationCloseDate = models.DateTimeField(null=True, blank=True)
    ReminderDate = models.DateTimeField(null=True, blank=True)
    PreTestCloseDate = models.DateTimeField(null=True, blank=True)
    PostTestCloseDate = models.DateTimeField(null=True, blank=True)
    EvaluationCloseDate = models.DateTimeField(null=True, blank=True)
    ParticipantGuide = models.ForeignKey(IDocument, null=True, blank=True)
    Flyer = models.FileField(upload_to='flyers/%Y/%m', null=True, blank=True)
    Language = models.ForeignKey(ILanguage, default=1)

    class Meta:
        db_table = 'Course_coursedelivery'
        app_label = 'v1'


class IEvaluation(models.Model):
    delivery = models.ForeignKey(IDelivery, db_column='CourseDelivery_id')
    q1 = models.CharField(max_length=2, null=True, blank=True)
    q2 = models.CharField(max_length=2, null=True, blank=True)
    q3 = models.CharField(max_length=2, null=True, blank=True)
    q4 = models.CharField(max_length=2, null=True, blank=True)
    q5 = models.CharField(max_length=2, null=True, blank=True)
    q6 = models.CharField(max_length=2, null=True, blank=True)
    q7 = models.CharField(max_length=2, null=True, blank=True)
    q8 = models.CharField(max_length=2, null=True, blank=True)
    q9 = models.CharField(max_length=2, null=True, blank=True)
    q10 = models.CharField(max_length=2, null=True, blank=True)
    q11 = models.CharField(max_length=2, null=True, blank=True)
    q12 = models.CharField(max_length=2, null=True, blank=True)
    q13 = models.CharField(max_length=2, null=True, blank=True)
    q14 = models.CharField(max_length=2, null=True, blank=True)
    q15 = models.CharField(max_length=2, null=True, blank=True)
    q16 = models.CharField(max_length=2, null=True, blank=True)
    q17 = models.CharField(max_length=2, null=True, blank=True)
    q18 = models.CharField(max_length=2, null=True, blank=True)
    q19 = models.CharField(max_length=2, null=True, blank=True)
    q20 = models.CharField(max_length=2, null=True, blank=True)
    q21 = models.CharField(max_length=2, null=True, blank=True)
    q22 = models.CharField(max_length=2, null=True, blank=True)
    q23 = models.CharField(max_length=2, null=True, blank=True)
    q24 = models.TextField(null=True, blank=True)
    q25 = models.TextField(null=True, blank=True)
    q26 = models.TextField(null=True, blank=True)
    q27 = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'Course_evaluation'
        app_label = 'v1'


class IParticipant(models.Model):
    CourseDelivery = models.ForeignKey(IDelivery)
    Person = models.ForeignKey(IPerson)
    AttendedCourse = models.BooleanField()
    TakenPreTest = models.BooleanField()
    TakenPostTest = models.IntegerField(null=False)
    TakenEvaluation = models.BooleanField()
    RegisteredDate = models.DateField()
    Passed = models.BooleanField()

    class Meta:
        db_table = 'Course_participant'
        app_label = 'v1'


class ITestResults(models.Model):
    Participant = models.ForeignKey(IParticipant)
    Test = models.ForeignKey(ITest)
    Question = models.ForeignKey(IQuestion)
    Answer = models.ForeignKey(IAnswer, null=True)
    Attempt = models.IntegerField(default=1)
    Latest = models.BooleanField(default=True)

    class Meta:
        db_table = 'Course_testresults'
        app_label = 'v1'


class ITestScore(models.Model):
    Participant = models.ForeignKey(IParticipant)
    Type = models.ForeignKey(ITestType)
    Score = models.IntegerField()
    Attempt = models.IntegerField(default=1)
    Latest = models.BooleanField(default=True)

    class Meta:
        db_table = 'Course_testscore'
        app_label = 'v1'


class IDeliveryTests(models.Model):
    CourseDelivery = models.ForeignKey(IDelivery)
    Test = models.ForeignKey(ITest)

    class Meta:
        db_table = 'Course_coursedeliverytest'
        app_label = 'v1'


class IDeliveryRequest(models.Model):
    User = models.ForeignKey(IUser)
    SubmitDate = models.DateTimeField()
    Course = models.ForeignKey(ICourse)
    Location = models.ForeignKey(IAddress, related_name='location')
    StartDate = models.DateTimeField()
    EndDate = models.DateTimeField()
    ParticipantCount = models.IntegerField()
    Agency1 = models.CharField(max_length=50)
    ContactName1 = models.CharField(max_length=50)
    Location1 = models.ForeignKey(IAddress, related_name='primary')
    WorkPhone1 = models.CharField(max_length=25)
    Cell1 = models.CharField(max_length=25, null=True, blank=True)
    Fax1 = models.CharField(max_length=25, null=True, blank=True)
    Email1 = models.CharField(max_length=200)
    Agency2 = models.CharField(max_length=50, null=True, blank=True)
    ContactName2 = models.CharField(max_length=50, null=True, blank=True)
    Location2 = models.ForeignKey(IAddress, null=True, blank=True, related_name='secondary')
    WorkPhone2 = models.CharField(max_length=25, null=True, blank=True)
    Cell2 = models.CharField(max_length=25, null=True, blank=True)
    Fax2 = models.CharField(max_length=25, null=True, blank=True)
    Email2 = models.CharField(max_length=200, null=True, blank=True)
    RegContactName = models.CharField(max_length=100, null=True, blank=True)
    RegWorkPhone = models.CharField(max_length=25, null=True, blank=True)
    RegEmail = models.CharField(max_length=200)
    Flyer = models.BooleanField()
    RegistrationCode = models.BooleanField()
    Clearance = models.BooleanField()
    ClearanceDetails = models.CharField(max_length=200, null=True, blank=True)
    Paperwork = models.BooleanField()
    PaperworkDetails = models.CharField(max_length=200, null=True, blank=True)
    ClassroomFacility = models.CharField(max_length=50)
    FacilityAddress = models.ForeignKey(IAddress, related_name='facility')
    FacilityContact = models.CharField(max_length=50)
    FacilityPhone = models.CharField(max_length=25)
    FacilityEmail = models.CharField(max_length=200)
    FacilityShippingAddress = models.ForeignKey(IAddress, null=True, related_name='shipping')
    ShippingContact = models.CharField(max_length=50, null=True, blank=True)
    ShippingContactPhone = models.CharField(max_length=25, null=True, blank=True)
    ShippingContactEmail = models.CharField(max_length=200, null=True, blank=True)
    InstructorAccessTime = models.TimeField()
    ParticipantAccessTime = models.TimeField()
    ComputerLab = models.BooleanField()
    InstructorLaptop = models.BooleanField()
    InstructorProjectionScreen = models.BooleanField()
    InstructorProjector = models.BooleanField()
    InstructorInternetHard = models.BooleanField()
    InstructorInternetWireless = models.BooleanField()
    InstructorSpeakers = models.BooleanField()
    InstructorPodium = models.BooleanField()
    AVSupport = models.BooleanField()
    RegistrationName = models.CharField(max_length=50, null=True, blank=True)
    RegistrationPhone = models.CharField(max_length=25, null=True, blank=True)
    AVName = models.CharField(max_length=50, null=True, blank=True)
    AVPhone = models.CharField(max_length=25, null=True, blank=True)
    Parking = models.BooleanField()
    ParkingRequirements = models.CharField(max_length=200, null=True, blank=True)
    Secure = models.BooleanField()
    Food = models.BooleanField()
    FoodDetails = models.CharField(max_length=200, null=True, blank=True)
    Beverage = models.BooleanField()
    Vending = models.BooleanField()
    Airport = models.CharField(max_length=100, null=True, blank=True)
    AirportDistance = models.FloatField()
    Hotel = models.CharField(max_length=100, null=True, blank=True)
    HotelDistance = models.FloatField()

    class Meta:
        db_table = 'Course_coursedeliveryrequest'
        app_label = 'v1'


class IAfterActionReport(models.Model):
    CourseDelivery = models.ForeignKey(IDelivery)
    Participants_A = models.IntegerField(null=True)
    Participants_B = models.IntegerField(null=True)
    Facilities_C = models.IntegerField(null=True)
    Facilities_D = models.IntegerField(null=True)
    Materials_E = models.IntegerField(null=True)
    Materials_F = models.IntegerField(null=True)
    Materials_F_Other = models.TextField(null=True)
    Materials_G_1 = models.IntegerField(null=True)
    Materials_G_2 = models.IntegerField(null=True)
    Materials_G_3 = models.IntegerField(null=True)
    Materials_G_4 = models.IntegerField(null=True)
    Materials_G_Other = models.TextField(null=True)
    Materials_H = models.IntegerField(null=True)
    Other_I = models.IntegerField(null=True)
    Other_Other = models.TextField(null=True)
    Instructor_A = models.IntegerField(null=True)
    Instructor_A_Conflict = models.IntegerField(null=True)
    Instructor_A_Explain = models.TextField(null=True)
    Instructor_B = models.IntegerField(null=True)
    Instructor_B_Comments = models.TextField(null=True)
    Course_A = models.IntegerField(null=True)
    Course_B = models.IntegerField(null=True)
    Course_B_Comments = models.TextField(null=True)
    Course_C = models.IntegerField(null=True)
    Course_D = models.IntegerField(null=True)
    Course_E = models.IntegerField(null=True)
    Course_F = models.IntegerField(null=True)
    Course_F_Comments = models.TextField(null=True)
    Admin_A = models.IntegerField(null=True)
    Admin_B = models.IntegerField(null=True)
    Admin_C = models.IntegerField(null=True)
    Admin_D = models.IntegerField(null=True)
    Admin_E = models.IntegerField(null=True)
    Admin_Comments = models.TextField(null=True)
    General_A = models.IntegerField(null=True)
    General_B = models.IntegerField(null=True)
    General_C = models.IntegerField(null=True)
    General_D = models.IntegerField(null=True)
    General_Comments_1 = models.TextField(null=True)
    General_Comments_2 = models.TextField(null=True)
    General_Comments_3 = models.TextField(null=True)
    General_Comments_4_1 = models.CharField(max_length=1, null=True)
    General_Comments_4_2 = models.TextField(null=True)
    General_Comments_5 = models.TextField(null=True)
    Status = models.IntegerField(null=True)
    UpdateUser = models.ForeignKey(IUser, null=True)
    UpdateDate = models.DateField(null=True)

    class Meta:
        db_table = 'Instructor_afteractionreport'
        app_label = 'v1'