from accounts.models import IPerson
from deliveries.models import IDelivery, IEvaluation, IParticipant, ITestResults, ITestScore, IDeliveryTests, IDeliveryRequest, IAfterActionReport
from ndptc.courses.models import Course, Test
from ndptc.deliveries.models import TrainingMethod, DeliveryType, DeliveryRequest, Delivery, Evaluation, AfterActionReport
from ndptc.participants.models import Participant, TestResult, TestScore
from ndptc.utilities.models import State, Country, Language
from pytz import timezone
from utilities.models import IAddress


hst = timezone('Pacific/Honolulu')


def import_delivery_data():
    import_deliveries()
    import_evaluations()
    import_participants()
    import_test_results()
    import_test_scores()
    import_delivery_tests()
    import_delivery_requests()
    import_after_action()


def get_timezone(City, State):
    return hst


def import_deliveries():
    for delivery in IDelivery.objects.all():
        course = Course.objects.get(id=delivery.Course_id)
        training_method = TrainingMethod.objects.get(id=delivery.TrainingMethod_id)
        location = IAddress.objects.get(id=delivery.Location_id)
        state = State.objects.get(id=location.State_id)
        country = Country.objects.get(id=location.Country_id)
        delivery_type = DeliveryType.objects.get(id=delivery.DeliveryType_id)
        language = Language.objects.get(id=delivery.Language_id)

        Delivery.objects.get_or_create(id=delivery.id, defaults={
            'course': course,
            'training_method': training_method,
            'delivery_type': delivery_type,
            'address': location.Address,
            'address1': location.Address1,
            'city': location.City,
            'state': state,
            'timezone': hst,
            'zip': location.Zip,
            'country': country,
            'start': delivery.StartDate,#make_aware(delivery.StartDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'end': delivery.EndDate,#make_aware(delivery.EndDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'pre_test_reminder': delivery.ReminderDate,#make_aware(delivery.ReminderDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'pre_test_close': delivery.PreTestCloseDate,#make_aware(delivery.PreTestCloseDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'post_test_close': delivery.PostTestCloseDate,#make_aware(delivery.PostTestCloseDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'evaluation_close': delivery.EvaluationCloseDate or delivery.EndDate,#make_aware(delivery.EvaluationCloseDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'delivery_closed': delivery.FinishDate,#make_aware(delivery.FinishDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'registration_close': delivery.RegistrationCloseDate,#make_aware(delivery.RegistrationCloseDate, get_timezone(delivery.Location.City, delivery.Location.State)),
            'confirmation_code': delivery.ConfirmationCode,
            'reported': delivery.Reported,
            'status': delivery.Status,
            'hosting_agency': delivery.HostingAgency,
            'maximum_enrollment': delivery.MaximumEnrollment,
            'contact_information': delivery.ContactInformation,
            'arrival_time': delivery.ArrivalTime,
            'course_materials': delivery.CourseMaterials,
            'internet': delivery.Internet,
            'food': delivery.Food,
            'parking': delivery.Parking,
            'requirements': delivery.Requirements,
            'additional_information': delivery.AdditionalInformation,
            'reading_materials': delivery.ReadingMaterials,
            'special_note': delivery.SpecialNote,
            'certificates_sent': delivery.CertificateEmailsSent,
            'language': language,
            'participant_guide_id': delivery.ParticipantGuide_id,
            'flyer': delivery.Flyer,
        })


def import_evaluations():
    for evaluation in IEvaluation.objects.all():
        delivery = Delivery.objects.get(id=evaluation.delivery.id)
        Evaluation.objects.get_or_create(id=evaluation.id, defaults={
            'delivery': delivery,
            'q1': evaluation.q1 if evaluation.q1 and evaluation.q1.isdigit() else None,
            'q2': evaluation.q2 if evaluation.q2 and evaluation.q2.isdigit() else None,
            'q3': evaluation.q3 if evaluation.q3 and evaluation.q3.isdigit() else None,
            'q4': evaluation.q4 if evaluation.q4 and evaluation.q4.isdigit() else None,
            'q5': evaluation.q5 if evaluation.q5 and evaluation.q5.isdigit() else None,
            'q6': evaluation.q6 if evaluation.q6 and evaluation.q6.isdigit() else None,
            'q7': evaluation.q7 if evaluation.q7 and evaluation.q7.isdigit() else None,
            'q8': evaluation.q8 if evaluation.q8 and evaluation.q8.isdigit() else None,
            'q9': evaluation.q9 if evaluation.q9 and evaluation.q9.isdigit() else None,
            'q10': evaluation.q10 if evaluation.q10 and evaluation.q10.isdigit() else None,
            'q11': evaluation.q11 if evaluation.q11 and evaluation.q11.isdigit() else None,
            'q12': evaluation.q12 if evaluation.q12 and evaluation.q12.isdigit() else None,
            'q13': evaluation.q13 if evaluation.q13 and evaluation.q13.isdigit() else None,
            'q14': evaluation.q14 if evaluation.q14 and evaluation.q14.isdigit() else None,
            'q15': evaluation.q15 if evaluation.q15 and evaluation.q15.isdigit() else None,
            'q16': evaluation.q16 if evaluation.q16 and evaluation.q16.isdigit() else None,
            'q17': evaluation.q17 if evaluation.q17 and evaluation.q17.isdigit() else None,
            'q18': evaluation.q18 if evaluation.q18 and evaluation.q18.isdigit() else None,
            'q19': evaluation.q19 if evaluation.q19 and evaluation.q19.isdigit() else None,
            'q20': evaluation.q20 if evaluation.q20 and evaluation.q20.isdigit() else None,
            'q21': evaluation.q21 if evaluation.q21 and evaluation.q21.isdigit() else None,
            'q22': evaluation.q22 if evaluation.q22 and evaluation.q22.isdigit() else None,
            'q23': evaluation.q23 if evaluation.q23 and evaluation.q23.isdigit() else None,
            'q24': evaluation.q24,
            'q25': evaluation.q25,
            'q26': evaluation.q26,
            'q27': evaluation.q27,
        })


def import_participants():
    for participant in IParticipant.objects.all():
        try:
            participant = Participant.objects.get(id=participant.id)
        except Participant.DoesNotExist:
            kwargs={
                'id': participant.id,
                'delivery_id': participant.CourseDelivery_id,
                'person_id': participant.Person_id,
                'pre_test': participant.TakenPreTest,
                'post_test': participant.TakenPostTest,
                'attended': participant.AttendedCourse,
                'evaluation': participant.TakenEvaluation,
                'registered': participant.RegisteredDate,
                'passed': participant.Passed,
            }
            new_participant = Participant(**dict((k,v) for (k,v) in kwargs.items() if '__' not in k))
            new_participant.save(force=True)


def import_test_results():
    for result in ITestResults.objects.all():
        try:
            TestResult.objects.get(id=result.id)
        except TestResult.DoesNotExist:
            kwargs = {
                'id': result.id,
                'question_id': result.Question_id,
                'answer_id': result.Answer_id,
                'participant_id': result.Participant_id,
                'attempt': result.Attempt,
                'is_latest': result.Latest,
            }
            result = TestResult(**kwargs)
            result.save(force=True)


def import_test_scores():
    count = 0
    for score in ITestScore.objects.all():
        s = TestScore.objects.get_or_create(id=score.id, defaults={
            'type_id': score.Type.Type,
            'score': score.Score,
            'is_latest': score.Latest,
            'attempt': score.Attempt,
        })

        try:
            participant = Participant.objects.get(id=score.Participant_id)
            if not participant.scores.filter(id=s[0].id).exists():
                participant.scores.add(s[0])
        except Participant.DoesNotExist:
            print score.Participant_id
            count += 1
    print 'Count: %i' % count


def import_delivery_tests():
    for test in IDeliveryTests.objects.all():
        delivery = Delivery.objects.get(id=test.CourseDelivery_id)
        try:
            delivery.tests.get(id=test.Test_id)
        except Test.DoesNotExist:
            delivery.tests.add(Test.objects.get(id=test.Test_id))


def import_delivery_requests():
    for request in IDeliveryRequest.objects.all():
        course = Course.objects.get(id=request.Course.id)
        person = IPerson.objects.get(user=request.User_id)
        address_1 = address1_1 = city_1 = state_1 = zip_1 = country_1 = None
        address_2 = address1_2 = city_2 = state_2 = zip_2 = country_2 = None
        address_3 = address1_3 = city_3 = state_3 = zip_3 = country_3 = None
        facility_address = facility_address1 = facility_city = facility_state = facility_zip = facility_country = None
        facility_shipping_address = facility_shipping_address1 = facility_shipping_city = facility_shipping_state = facility_shipping_zip = facility_shipping_country = None
        if request.Location:
            address_1 = request.Location.Address
            address1_1 = request.Location.Address1
            city_1 = request.Location.City
            state_1 = request.Location.State_id
            zip_1 = request.Location.Zip
            country_1 = request.Location.Country_id
        if request.Location1:
            address_2 = request.Location1.Address
            address1_2 = request.Location1.Address1
            city_2 = request.Location1.City
            state_2 = request.Location1.State_id
            zip_2 = request.Location1.Zip
            country_2 = request.Location1.Country_id
        if request.Location2:
            address_3 = request.Location2.Address
            address1_3 = request.Location2.Address1
            city_3 = request.Location2.City
            state_3 = request.Location2.State_id
            zip_3 = request.Location2.Zip
            country_3 = request.Location2.Country_id
        if request.FacilityAddress:
            facility_address = request.FacilityAddress.Address
            facility_address1 = request.FacilityAddress.Address1
            facility_city = request.FacilityAddress.City
            facility_state = request.FacilityAddress.State_id
            facility_zip = request.FacilityAddress.Zip
            facility_country = request.FacilityAddress.Country_id
        if request.FacilityShippingAddress:
            facility_shipping_address = request.FacilityShippingAddress.Address
            facility_shipping_address1 = request.FacilityShippingAddress.Address1
            facility_shipping_city = request.FacilityShippingAddress.City
            facility_shipping_state = request.FacilityShippingAddress.State_id
            facility_shipping_zip = request.FacilityShippingAddress.Zip
            facility_shipping_country = request.FacilityShippingAddress.Country_id

        DeliveryRequest.objects.get_or_create(id=request.id, defaults={
            'user_id': request.User_id,
            #course logistics
            'submit': request.SubmitDate,
            'course': course,
            'address_1': address_1,
            'address1_1': address1_1,
            'city_1': city_1,
            'state_1_id': state_1,
            'zip_1': zip_1,
            'country_1_id': country_1,
            'start': request.StartDate,
            'end': request.EndDate,
            'participant_count': request.ParticipantCount,
            #Hosting Agency 1
            'agency1': request.Agency1,
            'contact_name1': request.ContactName1,
            'address_2': address_2,
            'address1_2': address1_2,
            'city_2': city_2,
            'state_2_id': state_2,
            'zip_2': zip_2,
            'country_2_id': country_2,
            'phone1': request.WorkPhone1,
            'cell1': request.Cell1,
            'fax1': request.Fax1,
            'email1': request.Email1,
            #Hosting Agency 2
            'agency2': request.Agency2,
            'contact_name2': request.ContactName2,
            'address_3': address_3,
            'address1_3': address1_3,
            'city_3': city_3,
            'state_3': state_3,
            'zip_3': zip_3,
            'country_3_id': country_3,
            'phone2': request.WorkPhone2,
            'cell2': request.Cell2,
            'fax2': request.Fax2,
            'email2': request.Email2,
            #Individual Registering & Administrative
            'reg_contact_name': request.RegContactName,
            'reg_phone': request.RegWorkPhone,
            'reg_email': request.RegEmail,
            'flyer': request.Flyer,
            'registration_code': request.RegistrationCode,
            'clearance': request.Clearance,
            'clearance_details': request.ClearanceDetails,
            'paperwork': request.Paperwork,
            'paperwork_details': request.PaperworkDetails,
            #Training Facility
            'classroom_facility': request.ClassroomFacility,
            'facility_address': facility_address,
            'facility_address1': facility_address1,
            'facility_city': facility_city,
            'facility_state_id': facility_state,
            'facility_zip': facility_zip,
            'facility_country_id': facility_country,
            'facility_contact': request.FacilityContact,
            'facility_phone': request.FacilityPhone,
            'facility_email': request.FacilityEmail,
            #Training Facility Shipping
            'facility_shipping_address': facility_shipping_address,
            'facility_shipping_address1': facility_shipping_address1,
            'facility_shipping_city': facility_shipping_city,
            'facility_shipping_state_id': facility_shipping_state,
            'facility_shipping_zip': facility_shipping_zip,
            'facility_shipping_country_id': facility_shipping_country,
            'shipping_contact': request.ShippingContact,
            'shipping_contact_phone': request.ShippingContactPhone,
            'shipping_contact_email': request.ShippingContactEmail,
            #General Information
            'instructor_access_time': request.InstructorAccessTime,
            'participant_access_time': request.ParticipantAccessTime,
            'computer_lab': request.ComputerLab,
            'instructor_laptop': request.InstructorLaptop,
            'instructor_projection_screen': request.InstructorProjectionScreen,
            'instructor_projector': request.InstructorProjector,
            'instructor_internet_hard': request.InstructorInternetHard,
            'instructor_internet_wireless': request.InstructorInternetWireless,
            'instructor_speakers': request.InstructorSpeakers,
            'instructor_podium': request.InstructorPodium,
            'av_support': request.AVSupport,
            'registration_name': request.RegistrationName,
            'registration_phone': request.RegistrationPhone,
            'av_name': request.AVName,
            'av_phone': request.AVPhone,
            'parking': request.Parking,
            'parking_requirements': request.ParkingRequirements,
            'secure': request.Secure,
            'food': request.Food,
            'food_details': request.FoodDetails,
            'beverage': request.Beverage,
            'vending': request.Vending,
            'airport': request.Airport,
            'airport_distance': request.AirportDistance,
            'hotel': request.Hotel,
            'hotel_distance': request.HotelDistance
        })


def import_after_action():
    for report in IAfterActionReport.objects.all():
        AfterActionReport.objects.get_or_create(id=report.id, defaults={
            'delivery_id': report.CourseDelivery_id,
            'participants_a': report.Participants_A,
            'participants_b': report.Participants_B,
            'facilities_c': report.Facilities_C,
            'facilities_d': report.Facilities_D,
            'materials_e': report.Materials_E,
            'materials_f': report.Materials_F,
            'materials_f_other': report.Materials_F_Other,
            'materials_g_1': report.Materials_G_1,
            'materials_g_2': report.Materials_G_2,
            'materials_g_3': report.Materials_G_3,
            'materials_g_4': report.Materials_G_4,
            'materials_g_other': report.Materials_G_Other,
            'materials_h': report.Materials_H,
            'other_i': report.Other_I,
            'other_other': report.Other_Other,
            'instructor_a': report.Instructor_A,
            'instructor_a_conflict': report.Instructor_A_Conflict,
            'instructor_a_explain': report.Instructor_A_Explain,
            'instructor_b': report.Instructor_B,
            'instructor_b_comments': report.Instructor_B_Comments,
            'course_a': report.Course_A,
            'course_b': report.Course_B,
            'course_b_comments': report.Course_B_Comments,
            'course_c': report.Course_C,
            'course_d': report.Course_D,
            'course_e': report.Course_E,
            'course_f': report.Course_F,
            'course_f_comments': report.Course_F_Comments,
            'admin_a': report.Admin_A,
            'admin_b': report.Admin_B,
            'admin_c': report.Admin_C,
            'admin_d': report.Admin_D,
            'admin_e': report.Admin_E,
            'admin_comments': report.Admin_Comments,
            'general_a': report.General_A,
            'general_b': report.General_B,
            'general_c': report.General_C,
            'general_d': report.General_D,
            'general_comments_1': report.General_Comments_1,
            'general_comments_2': report.General_Comments_2,
            'general_comments_3': report.General_Comments_3,
            'general_comments_4_1': report.General_Comments_4_1,
            'general_comments_4_2': report.General_Comments_4_2,
            'general_comments_5': report.General_Comments_5
        })



