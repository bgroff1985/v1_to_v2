from django.db import models
import uuid

class IDiscipline(models.Model):
    Description = models.CharField(max_length=100)
    Code = models.CharField(max_length=5, unique=True)

    class Meta:
        db_table = 'Utilities_discipline'
        app_label = 'v1'


class IGovernmentLevel(models.Model):
    Description = models.CharField(max_length=100)
    Code = models.CharField(max_length=2, unique=True)
    SortOrder = models.IntegerField()

    class Meta:
        db_table = 'Utilities_governmentlevel'
        app_label = 'v1'


class ICountry(models.Model):
    ISOCode = models.CharField(max_length=3, unique=True)
    Name = models.CharField(max_length=50)
    FemaCode = models.CharField(max_length=2, unique=True)
    SortOrder = models.IntegerField()

    class Meta:
        db_table = 'Utilities_country'
        app_label = 'v1'


class IState(models.Model):
    Name = models.CharField(max_length=50)
    FemaCode = models.CharField(max_length=2, unique=True)
    FemaRegion = models.IntegerField()

    class Meta:
        db_table = 'Utilities_state'
        app_label = 'v1'


class IAddress(models.Model):
    Address = models.CharField(max_length=100, null=True)
    Address1 = models.CharField(max_length=100, null=True)
    City = models.CharField(max_length=30)
    State = models.ForeignKey(IState)
    Zip = models.CharField(max_length=25)
    Country =  models.ForeignKey(ICountry, null=True)

    class Meta:
        db_table = 'Utilities_address'
        app_label = 'v1'


class ILanguage(models.Model):
    Code = models.CharField(max_length=4)
    Language = models.CharField(max_length=50)

    class Meta:
        db_table = 'Utilities_language'
        app_label = 'v1'

class ILevelThreeSurvey(models.Model):
    Participant = models.ForeignKey('Course.Participant')
    Uuid = models.CharField(max_length=255)
    Taken = models.BooleanField()
    Question1 = models.TextField(null=True)
    Question2 = models.TextField(null=True)
    Question3 = models.CharField(max_length=15)
    Question4 = models.TextField(null=True)
    Question5 = models.TextField(null=True)
    Question6 = models.TextField(null=True)
    Question7 = models.TextField(null=True)
    Question8 = models.TextField(null=True)
    Question9 = models.TextField(null=True)
    Question10 = models.CharField(max_length=2)
    Question11 = models.TextField(null=True)
    Question12 = models.TextField(null=True)
    Question13 = models.TextField()
    Question14 = models.TextField(null=True)
    Question15 = models.TextField(null=True)
    Question16 = models.TextField(null=True)
    Question17 = models.TextField(blank=True, null=True)
    Question18 = models.TextField(blank=True, null=True)
    Question19 = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'Utilities_levelthreesurvey'
        app_label = 'v1'