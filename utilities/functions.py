from ndptc.utilities.models import State, Country, Language, GovernmentLevel, Discipline
from ndptc.participants.models import LevelThreeSurvey
from utilities.models import IState, ICountry, ILanguage, IGovernmentLevel, IDiscipline, ILevelThreeSurvey


def import_utilities():
    import_states()
    import_countries()
    import_languages()
    import_government_levels()
    import_disciplines()


def import_states():
    for state in IState.objects.all():
        State.objects.get_or_create(id=state.id, defaults={
            'name': state.Name,
            'fema_code': state.FemaCode,
            'fema_region': state.FemaRegion,
        })


def import_countries():
    for country in ICountry.objects.all():
        Country.objects.get_or_create(id=country.id, defaults={
            'iso_code': country.ISOCode,
            'name': country.Name,
            'fema_code': country.FemaCode,
            'sort_order': country.SortOrder,
        })


def import_languages():
    for language in ILanguage.objects.all():
        Language.objects.get_or_create(id=language.id, defaults={
            'code': language.Code,
            'language': language.Language,
        })


def import_government_levels():
    for government_level in IGovernmentLevel.objects.all():
        GovernmentLevel.objects.get_or_create(id=government_level.id, description=government_level.Description,
                                               code=government_level.Code, sort_order=government_level.SortOrder)


def import_disciplines():
    for discipline in IDiscipline.objects.all():
        Discipline.objects.get_or_create(id=discipline.id, description=discipline.Description,
                                          code=discipline.Code)

def import_levelthreesurveys():
    for survey in ILevelThreeSurvey.objects.all():
        LevelThreeSurvey.get_or_create(id=survey.id, uuid=survey.Uuid, participant=survey.Participant, taken=survey.Taken,
                                       question1=survey.Question1, question2=survey.Question2, question3=survey.Question3,
                                       question4=survey.Question4, question1=survey.Question5, question1=survey.Question6,
                                       question7=survey.Question7, question8=survey.Question8, question9=survey.Question9,
                                       question10=survey.Question10, question11=survey.Question11, question12=survey.Question12,
                                       question13=survey.Question13, question14=survey.Question14, question15=survey.Question15,
                                       question16=survey.Question16, question17=survey.Question17, question18=survey.Question18,
                                       question19=survey.Question19)