from django.db import models


class IEmploymentStatus(models.Model):
    Type = models.CharField(max_length=20, unique=True)
    Code = models.CharField(max_length=2, unique=True)

    class Meta:
        db_table = 'Instructor_employmentstatus'
        app_label = 'v1'


class IInstructor(models.Model):
    Person = models.ForeignKey('courses.IPerson')
    EmploymentStatus = models.ForeignKey(IEmploymentStatus, null=True, blank=True)
    EffectiveDate = models.DateField(null=True, blank=True)
    BackgroundCheck = models.BooleanField(default=False)
    BackgroundCheckDate = models.DateField(null=True, blank=True)
    Comments = models.TextField(null=True, blank=True)
    Active = models.BooleanField()
    Status = models.IntegerField()
    Bio = models.TextField(null=True, blank=True)
    Image = models.ImageField(upload_to='instructor_images/', null=True, blank=True)
    Resume = models.FileField()

    class Meta:
        db_table = 'Instructor_instructor'
        app_label = 'v1'


class IInstructorType(models.Model):
    Type = models.CharField(max_length=20, unique=True)
    Code = models.CharField(max_length=2, unique=True)

    class Meta:
        db_table = 'Instructor_instructortype'
        app_label = 'v1'


class IAvailable(models.Model):
    Instructor = models.ForeignKey('IInstructor')
    CourseDelivery = models.ForeignKey('deliveries.IDelivery')

    class Meta:
        db_table = 'Instructor_available'
        app_label = 'v1'


class ITaught(models.Model):
    Instructor = models.ForeignKey(IInstructor)
    CourseDelivery = models.ForeignKey('deliveries.Deliveries')
    InstructorType = models.ForeignKey(IInstructorType)

    class Meta:
        db_table = 'Instructor_coursedeliveryinstructor'
        app_label = 'v1'


class ICertification(models.Model):
    Instructor = models.ForeignKey(IInstructor)
    Course = models.ForeignKey('courses.ICourse')
    InstructorType = models.ForeignKey(IInstructorType)
    SubjectExpert = models.BooleanField(default=False)
    EmergencyManagement = models.BooleanField(default=False)
    DateCertified = models.DateField()

    class Meta:
        db_table = 'Instructor_instructorcoursecertification'
        app_label = 'v1'


class IInstructorEvaluation(models.Model):
    """
    """
    CourseDeliveryInstructor = models.ForeignKey(ITaught)
    Question1 = models.CharField(max_length=2, null=True)
    Question2 = models.CharField(max_length=2, null=True)
    Question3 = models.CharField(max_length=2, null=True)
    Question4 = models.CharField(max_length=2, null=True)
    Question5 = models.CharField(max_length=2, null=True)
    Question6 = models.CharField(max_length=2, null=True)
    Comment = models.TextField(null=True)

    class Meta:
        db_table = 'Instructor_instructorevaluation'
        app_label = 'v1'
