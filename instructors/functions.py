from ndptc.deliveries.models import InstructorEvaluation
from instructors.models import *
from ndptc.instructors.models import *


def import_instructor_data():
#    import_employment_status()
#    import_instructor_types()
    import_instructors()
    import_availability()
    import_teachings()
    import_certifications()
    import_instructor_evaluations()


def import_employment_status():
    for status in IEmploymentStatus.objects.all():
        EmploymentStatus.objects.get_or_create(id=status.id, defaults={
            'status': status.Type,
            'code': status.Code,
        })


def import_instructor_types():
    for type in IInstructorType.objects.all():
        InstructorType.objects.get_or_create(id=type.id, defaults={
            'description': type.Type,
            'code': type.Code,
        })


def import_instructors():
    for instructor in IInstructor.objects.all():
        Instructor.objects.get_or_create(id=instructor.id, defaults={
            'person_id': instructor.Person_id,
            'employment_status_id': instructor.EmploymentStatus_id,
            'effective_date': instructor.EffectiveDate,
            'background_check': instructor.BackgroundCheck,
            'background_date': instructor.BackgroundCheckDate,
            'comments': instructor.Comments,
            'bio': instructor.Bio,
            'image': instructor.Image,
            'resume': instructor.Resume,
            'is_active': instructor.Active,
        })


def import_availability():
    for available in IAvailable.objects.all():
        Availability.objects.get_or_create(id=available.id, defaults={
            'instructor_id': available.Instructor_id,
            'delivery_id': available.CourseDelivery_id,
        })


def import_teachings():
    for taught in ITaught.objects.all():
        Taught.objects.get_or_create(id=taught.id, defaults={
            'instructor_id': taught.Instructor_id,
            'instructor_type_id': taught.InstructorType_id,
            'delivery_id': taught.CourseDelivery_id,
        })


def import_certifications():
    for cert in ICertification.objects.all():
        Certification.objects.get_or_create(id=cert.id, defaults={
            'instructor_id': cert.Instructor_id,
            'course_id': cert.Course_id,
            'subject_expert': cert.SubjectExpert,
            'emergency_management': cert.EmergencyManagement,
            'date_certified': cert.DateCertified,
        })


def import_instructor_evaluations():
    for evalauation in IInstructorEvaluation.objects.all():
        InstructorEvaluation.objects.get_or_create(id=evalauation.id, defaults={
            'instructor_id': evalauation.CourseDeliveryInstructor.Instructor_id,
            'delivery_id': evalauation.CourseDeliveryInstructor.CourseDelivery_id,
            'question1': evalauation.Question1 if evalauation.Question1 and evalauation.Question1.isdigit() else None,
            'question2': evalauation.Question2 if evalauation.Question2 and evalauation.Question2.isdigit() else None,
            'question3': evalauation.Question3 if evalauation.Question3 and evalauation.Question3.isdigit() else None,
            'question4': evalauation.Question4 if evalauation.Question4 and evalauation.Question4.isdigit() else None,
            'question5': evalauation.Question5 if evalauation.Question5 and evalauation.Question5.isdigit() else None,
            'question6': evalauation.Question6 if evalauation.Question6 and evalauation.Question6.isdigit() else None,
            'comment': evalauation.Comment,
        })
