class Router(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'v1':
            return 'ndptc'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'v1':
            return 'ndptc'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'v1' or \
           obj2._meta.app_label == 'v1':
           return True
        return None

    def allow_syncdb(self, db, model):
        return None
