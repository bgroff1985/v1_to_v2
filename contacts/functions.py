from contacts.models import IContactGroup, ContactGroup, IContact, Contact


def import_contact_groups():
    for group in IContactGroup.objects.all():
        ContactGroup.objects.get_or_create(pk=group.pk, defaults={
            'name': group.Name,
            'sort_order': group.SortOrder
        })


def import_contacts():
    import_contact_groups()
    for contact in IContact.objects.all():
        Contact.objects.get_or_create(pk=contact.pk, defaults={
            'last_name': contact.LastName,
            'first_name': contact.FirstName,
            'title': contact.Title,
            'phone': contact.Phone,
            'cell': contact.Cell,
            'email': contact.Email,
            'group_id': contact.Group.id,
            'sort_order': contact.SortOrder,
        })