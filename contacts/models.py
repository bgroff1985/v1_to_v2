from django.contrib.localflavor.us.models import PhoneNumberField
from django.db import models


class IContactGroup(models.Model):
    Name = models.CharField(max_length=40)
    SortOrder = models.IntegerField()

    class Meta:
        app_label = 'v1'
        db_table = 'Contacts_contactgroup'


class IContact(models.Model):
    LastName = models.CharField(max_length=20)
    FirstName = models.CharField(max_length=20)
    Title = models.CharField(max_length=200)
    Phone = PhoneNumberField(null=True, blank=True)
    Cell = PhoneNumberField(null=True, blank=True)
    Email = models.EmailField(null=True, blank=True)
    Group = models.ForeignKey(IContactGroup)
    SortOrder = models.IntegerField()

    class Meta:
        app_label = 'v1'
        db_table = 'Contacts_contact'


class ContactGroup(models.Model):
    name = models.CharField(max_length=40)
    sort_order = models.IntegerField()

    class Meta:
        ordering = ('sort_order', 'name')
        db_table = 'aku_contact_group'

    def __str__(self):
        return self.name


class Contact(models.Model):
    last_name = models.CharField(max_length=20)
    first_name = models.CharField(max_length=20)
    title = models.CharField(max_length=200)
    phone = PhoneNumberField(null=True, blank=True)
    cell = PhoneNumberField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    group = models.ForeignKey(ContactGroup)
    sort_order = models.IntegerField()

    class Meta:
        ordering = ('sort_order', 'last_name', 'first_name')
        db_table = 'aku_contact'
